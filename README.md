# traces-db

A repository of redistributable traces for use in testing Mesa.

If you have redistributable GL/vulkan/etc. rendering traces that Mesa
could integrate in automated testing, we would like it in this repo!

## Adding a trace:

- Keep traces under 500MB

Until we fix out freedesktop.org's egress costs, we need to keep the
traces stored here small.  This is an approximate rule, just consider
how important this trace is compared to the cost to store on fd.o and
move it to target devices.

- Use apitrace instead of renderdoc when possible.

apitrace is smaller for the same capture, faster to replay the trace,
needs fewer dependencies (no python runtime), and has more developer
tools for performance analysis.

- Keep as few frames as possible.

With apitrace, use `apitrace gltrim` to trim the trace down to just
what you need to demonstrate the rendering you want.

- Document the license

Include the COPYING/LICENSE/etc. file from the program justifying that
we can distribute this trace and store it in the program's directory
in the git tree.

- Document the version of the program

Someone debugging with your trace later will need to know what
specific version of the program is being run and how, in case updates
of the traces are necessary.  Include as much as you can in the commit
message to help someone reproduce with the real program.

## Modifying a trace:

Occasionally we may need to modify a trace -- updating the version of
the app, improving the trimming to reduce runtime, changing the
extensions used etc.  When doing so, you must give the trace a unique
filename (such as moving it to a `-v2`, `-v3`, etc.) because Mesa's
trace testing infrastructure caches traces by filename instead of git
hash or file hash.

## Merging changes:

- Get an ack or r-b statement on the MR from someone before assigning
  to Marge-bot.

Since this is a repo with limited impact on other developers when we
add new traces, we want the barrier to entry to be low.  No need to
git commit --amend in the r-b statement, or get broad consensus.  If
you're modifying an existing trace, you should probably get an ack
from the original committer.
